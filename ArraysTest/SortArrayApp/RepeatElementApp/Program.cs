﻿using System;
using System.Collections.Generic;

namespace RepeatElementApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = new[] { 5, 2, 9, 12, 56, 73, 9, 44 };

            int repeatedElement = GetRepeatedElement(array);

            Console.WriteLine($"Repeated element: {repeatedElement}");
            Console.ReadKey();
        }

        private static int GetRepeatedElement(int[] array)
        {
            var tempList = new List<int>();

            foreach (var el in array)
            {
                if (tempList.Contains(el))
                {
                    return el;
                }

                tempList.Add(el);
            }

            return -1;
        }
    }
}
