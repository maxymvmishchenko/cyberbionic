﻿using System;
using System.Collections.Generic;

namespace PalindromTimesApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = GetPalindromTimes();
            Print(result);
        }

        private static void Print(List<string> result)
        {
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }
        }

        private static List<string> GetPalindromTimes()
        {
            var palindromes = new List<string>();

            for (int hour = 0; hour < 24; hour++)
            {
                if (hour >= 6 && hour < 10 || hour >= 16 && hour < 20)
                {
                    continue;
                }

                var currentStringHour = hour > 9
                    ? hour.ToString()
                    : "0" + hour.ToString();

                palindromes.Add(string.Format($"{currentStringHour}:{currentStringHour[1]}{currentStringHour[0]}"));
            }

            return palindromes;
        }
    }
}