﻿using System;

namespace SortArrayApp
{
    class Program
    {
        static void Main(string[] args)
        {

            int[] array1 = new int[]
            {
                2, 4, 8, 34
            };

            int[] array2 = new int[]
            {
                2, 5, 6, 8, 12, 42, 54
            };
            
            var array3 = GetSortArray(array2, array1);
            Print(array3);

            Console.ReadKey();
        }

        private static void Print(int[] array3)
        {
            foreach (var element in array3)
            {
                Console.WriteLine(element);
            }
        }

        private static int[] GetSortArray(int[] array1, int[] array2)
        {
            int[] array3 = new int[array1.Length + array2.Length];

            for (int i = 0, a1Index = 0, a2Index = 0; i < array3.Length; i++)
            {
                if (a1Index >= array1.Length)
                {
                    array3[i] = array2[a2Index++];
                    continue;
                }

                if (a2Index >= array2.Length)
                {
                    array3[i] = array1[a1Index++];
                    continue;
                }

                if (array1[a1Index] <= array2[a2Index])
                {
                    array3[i] = array1[a1Index++];
                    continue;
                }

                array3[i] = array2[a2Index++];
            }

            return array3;
        }

    }
}