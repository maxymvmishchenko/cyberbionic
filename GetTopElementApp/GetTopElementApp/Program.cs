﻿using System;
using System.Collections.Generic;

namespace GetTopElementApp
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass list = new MyClass();
            var result = list.LargestX(3, new List<int> { 12, 9, 5, 4, 3, 2, 0 });
            list.Print(result);
            Console.Read();
        }
    }
}
