﻿using System;
using System.Collections.Generic;

namespace GetTopElementApp
{
    public class MyClass
    {
        public List<int> LargestX(int a, List<int> list)
        {
            list.Sort();
            list.Reverse();
            List<int> myList = new List<int>();
            for (int i = 0; i < list.Capacity; i++)
            {
                if (a > i)
                {
                    myList.Add(list[i]);
                }
            }
            myList.Reverse();
            return myList;
        }
        public void Print(List<int> newArray)
        {
            foreach (var elem in newArray)
            {
                Console.WriteLine(elem);
            }
        }
    }
}
