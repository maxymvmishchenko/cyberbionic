The Task 4

Using Visual Studio, create a project using the Console Application template.
Required:
Create classes Point and Figure.
The Point class must contain two integer fields and one string field.
Create three properties with one get accessor.
Create a custom constructor, in the body of which initialize the fields with the values ​​of the arguments. The Figure class must contain constructors that take 3 to 5 arguments of type Point.
Create two methods: double LengthSide (Point A, Point B), which calculates the side length of the polygon; void PerimeterCalculator (), which calculates the perimeter of the polygon.
Write a program that displays the name and perimeter of the polygon.