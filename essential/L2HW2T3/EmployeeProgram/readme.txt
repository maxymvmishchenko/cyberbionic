The Task 3

Using Visual Studio, create a project using the Console Application template.
Required:
Create class Employee.
In the body of the class, create a custom constructor that takes two string arguments and initializes the fields corresponding to the last name and first name of the employee.
Create a method for calculating an employee's salary (depending on position and experience) and tax collection.
Write a program that displays information about an employee (last name, first name, job title), salary and tax collection.