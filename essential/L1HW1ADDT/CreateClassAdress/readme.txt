The task
Using Visual Studio, create a project using the Console Application template.
Required:
Create a class named Address.
In the body of the class, you need to create fields: index, country, city, street, house, apartment. For each field, create a property with two accessors.
Create an instance of the Address class.
In the instance fields, write information about the postal address.
Display the values ​​of the fields that describe the address.


The solution
+Create a public class Adress.
+In the body of the class, create 6 private fields: index, country, city, street, house, apartment.
+Create a property for each field. The get property for writing, the set property for reading.

+Create a static class Program and in the body of the Main method create an instance of the class on the heap.
+Using the class instance, we initialize the variables of the Adress class.
+We display the value of the initialized variables.