﻿namespace CreateClassAdress
{
    using System;

    namespace CreateClassAdress
    {
        public class Adress
        {
            private string _index;

            public string Index
            {
                get { return _index; }
                set { _index = value; }
            }
            private string _country;

            public string Country
            {
                get { return _country; }
                set { _country = value; }
            }
            private string _city;

            public string City
            {
                get { return _city; }
                set { _city = value; }
            }
            private string _street;

            public string Street
            {
                get { return _street; }
                set { _street = value; }
            }
            private string _house;

            public string House
            {
                get { return _house; }
                set { _house = value; }
            }
            private string _flat;

            public string Flat
            {
                get { return _flat; }
                set { _flat = value; }
            }

        }

        public class Program
        {
            static void Main()
            {
                Adress place = new Adress();
                place.Index = "13300";
                place.Country = "Ukraine";
                place.City = "Berdichev";
                place.Street = "Odesska";
                place.House = "20";
                place.Flat = "18";

                Console.WriteLine("Mail Index - {0}", place.Index);
                Console.WriteLine("Country - {0}", place.Country);
                Console.WriteLine("City - {0}", place.City);
                Console.WriteLine("Street - {0}", place.Street);
                Console.WriteLine("House - {0}", place.House);
                Console.WriteLine("Flat - {0}", place.Flat);

                Console.ReadKey();

            }
        }

    }

}
