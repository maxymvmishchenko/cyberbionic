The Task 4

Using Visual Studio, create a project using the Console Application template.
Required
Create class Invoice.
In the body of the class, create three fields int account, string customer, string provider, which must be initialized once (when creating instances of this class) without the possibility of further changing them.
In the body of the class, create two private fields line article, int number
Create a method for calculating the cost of an order with and without VAT.
A program that displays the amount of payment for the ordered goods with or without VAT.