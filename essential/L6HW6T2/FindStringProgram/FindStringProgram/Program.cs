﻿using System;

namespace FindStringProgram
{
    class Book
    {
        public void FindNext(string str)
        {
            Console.WriteLine("Find string: " + str);
        }
    }
       static class FindAndReplaceManager
    {

        public static void FindNext(string str)
        {
            Book book = new Book();
            book.FindNext(str);
        }
    }
    class Program
    {
        static void Main()
        {
            FindAndReplaceManager.FindNext("Something new");

            Console.ReadKey();

        }
    }
}
