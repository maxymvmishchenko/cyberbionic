Task 2
Using Visual Studio, create a project using the Console Application template.
Required:
Create a static FindAndReplaceManager class with a void FindNext (string str) method for searching the book from the 005_Delegation example lesson. When this method is called, a sequential search for a string in the book is performed.




Solution:

 + Create the Book class
 + In the body of the class, we create a method named FindNext that takes 1 string argument and returns nothing.
 + In the body of the method, display the value of the argument to the screen.
 + Create a static FindAndReplaceManager class with a void FindNext (string str) method for searching through a book. 
 + In the body of the FindNext method, create an instance of the Book class.
 + On an instance of the class, call the FindNext method from the Book class and pass 1 argument of the string type.
 + In the body of the Program class on the FindAndReplaceManager object class, we call the FindNext method and pass the value as an argument parameter.