﻿using System;

namespace NotebookProgram
{
    public interface IRam
    {
        void ChoiceRam();

    }

    public class KingstonRam : IRam
    {
        public void ChoiceRam()
        {
            Console.WriteLine("On the board RAM - Kingston");
        }
    }  

    public class GoodRam : IRam
    {
        public void ChoiceRam()
        {
            Console.WriteLine("On the board RAM - GoodRam");
        }
    }

    public class TranscendRam : IRam
    {
        public void ChoiceRam()
        {
            Console.WriteLine("On the board RAM - Transcend");
        }
    }
}
