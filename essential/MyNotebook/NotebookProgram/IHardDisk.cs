﻿using System;

namespace NotebookProgram
{
    public interface IHardDisk
    {
        void ChoiceHardDisk();
    }

    public class SumsungHardDisk : IHardDisk
    {
        public void ChoiceHardDisk()
        {
            Console.WriteLine("On the board hard disk - Sumsung");
        }
    }

    public class ToshibaHardDisk : IHardDisk
    {
        public void ChoiceHardDisk()
        {
            Console.WriteLine("On the board hard disk - Toshiba");
        }
    }

    public class SeagateHardDisk : IHardDisk
    {
        public void ChoiceHardDisk()
        {
            Console.WriteLine("On the board hard disk - Seagate");
        }
    }
}