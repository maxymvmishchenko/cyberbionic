﻿using System;

namespace NotebookProgram
{
    public interface IGraphicsCard
    {
        void ChoiceGraphicsCard();
    }

    public class NVidiaGraphicsCard : IGraphicsCard
    {
        public void ChoiceGraphicsCard()
        {
         Console.WriteLine("On the board graphics card - NVidia");
        }
    }

    public class AmdGraphicsCard : IGraphicsCard
    {
        public void ChoiceGraphicsCard()
        {
            Console.WriteLine("On the board graphics card - AMD");
        }
    }

    public class GigabyteGraphicsCard : IGraphicsCard
    {
        public void ChoiceGraphicsCard()
        {
            Console.WriteLine("On the board graphics card - Gigabyte");
        }
    }
}
