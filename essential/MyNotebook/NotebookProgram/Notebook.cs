﻿using System;
using System.Runtime.InteropServices;

namespace NotebookProgram
{
    public class Notebook
    {
        private readonly ICpu _cpu;
        private readonly IRam _ram;
        private readonly IGraphicsCard _graphicsCard;
        private readonly IHardDisk _hardDisk;
        
        
        public Notebook(ICpu cpu, IRam ram, IGraphicsCard graphicsCard, IHardDisk hardDisk)
        {
            _cpu = cpu;
            _ram = ram;
            _graphicsCard = graphicsCard;
            _hardDisk = hardDisk; 
        }

        public void ChoiseRam()
        {
          _ram.ChoiceRam();
        }

        public void ChoiceHardDisk()
        {
            _hardDisk.ChoiceHardDisk();
        }

        public void ChoiceGraphicsCard()
        {
            _graphicsCard.ChoiceGraphicsCard();
        }

        public void ChoiceCpu()
        {
            _cpu.ChoiceCpu();
        }
    }
}
