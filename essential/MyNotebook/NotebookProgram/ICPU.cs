﻿using System;

namespace NotebookProgram
{
    public interface ICpu
    {
        void ChoiceCpu();
    }

    public class IntelCpu : ICpu
    {
        public void ChoiceCpu()
        {
            Console.WriteLine("On the board CPU - IntelCore7");
        }
    }

    public class AmdCpu : ICpu
    {
        public void ChoiceCpu()
        {
            Console.WriteLine("On the board CPU - AMd");
        }
    }

    public class IbmCpu : ICpu
    {
        public void ChoiceCpu()
        {
            Console.WriteLine("On the board CPU - IBM");
        }
    }
}
