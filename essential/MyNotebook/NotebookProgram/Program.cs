﻿using System;
using System.Linq.Expressions;

namespace NotebookProgram
{
    class Program
    {
        static void Main()
        {
            //CPU
            var intelCpu = new IntelCpu();
            var amdCpu = new AmdCpu();
            var ibmCpu = new IbmCpu();

            //RAM
            var kingstonRam = new KingstonRam();
            var goodRam = new GoodRam();
            var transcendRam = new TranscendRam();

            //Graphics Card
            var nVidiaGraphicsCard = new NVidiaGraphicsCard();
            var amdGraphicsCard = new AmdGraphicsCard();
            var gigabyteGraphicsCard = new GigabyteGraphicsCard();

            //HardDisk
            var sumsungHardDisk = new SumsungHardDisk();
            var toshibaHardDisk = new ToshibaHardDisk();
            var seagateHardDisk = new SeagateHardDisk();


            Console.WriteLine("Assemble your laptop: ");
            Console.WriteLine("Сhoose CPU: \n 1 - Intel; \n 2 - AMD; \n 3 - IBM;");
            var userChoiceCpu = Console.ReadLine();
            ICpu currentCpu = null;

        
            switch (userChoiceCpu)
            {
                case "1":
                {
                    currentCpu = intelCpu;
                    break;
                }

                case "2":
                {
                    currentCpu = amdCpu;
                    break;
                }

                case "3":
                {
                    currentCpu = ibmCpu;
                    break;
                }
            }

            Console.WriteLine("Сhoose RAM: \n 1 - Kingston; \n 2 - GoodRam; \n 3 - Transcend;");
            var userChoiceRam = Console.ReadLine();
            IRam currentRam = null;

            switch (userChoiceRam)
            {
                case "1":
                {
                    currentRam = kingstonRam;
                    break;
                }

                case "2":
                {
                    currentRam = goodRam;
                    break;
                }

                case "3":
                {
                    currentRam = transcendRam;
                    break;
                }
            }

            Console.WriteLine("Сhoose Graphic Card: \n 1 - Nvidia; \n 2 - AMD; \n 3 - Gigabyte;");
            var userChoiceGraphicsCard = Console.ReadLine();
            IGraphicsCard currentGraphicsCard = null;

            switch (userChoiceGraphicsCard)
            {
                case "1":
                {
                    currentGraphicsCard = amdGraphicsCard;
                    break;
                }

                case "2":
                {
                    currentGraphicsCard = nVidiaGraphicsCard;
                    break;
                }

                case "3":
                {
                    currentGraphicsCard = gigabyteGraphicsCard;
                    break;
                }
            }

            Console.WriteLine("Сhoose HardDisk: \n 1 - Sumsung; \n 2 - Toshiba; \n 3 - Seagate;");
            var userChoiceHardDisk = Console.ReadLine();
            IHardDisk currentHardDisk = null;

            switch (userChoiceHardDisk)
            {
                case "1":
                {
                    currentHardDisk = sumsungHardDisk;
                    break;
                }

                case "2":
                {
                    currentHardDisk = toshibaHardDisk;
                    break;
                }

                case "3":
                {
                    currentHardDisk = seagateHardDisk;
                    break;
                }
            }

            var myNotebook = new Notebook(currentCpu, currentRam, currentGraphicsCard, currentHardDisk);
            Console.WriteLine("Your choice: ");
            myNotebook.ChoiceCpu();
            myNotebook.ChoiseRam();
            myNotebook.ChoiceGraphicsCard();
            myNotebook.ChoiceHardDisk();

            Console.ReadKey();
        }
    }
}
