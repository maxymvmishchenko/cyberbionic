﻿using System;

namespace ThreePointStructApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Point aPoint = new Point(1, 1);
            Point bPoint = new Point(2, 2);

            Point c = aPoint + bPoint;

            Console.WriteLine(c.ToString());
            Console.ReadKey();
        }
    }
}
