﻿namespace ThreePointStructApp
{
    struct Point
    {
        private int _x, _y;

        public Point(int xValue, int yValue)
        {
            _x = xValue;
            _y = yValue;
        }

        public static Point operator +(Point p1, Point p2)
        {
            return new Point(p1._x + p2._x, p1._y + p2._y);
        }

        public override string ToString()
        {
            return string.Format($"{_x}, {_y}");
        }
    }
}