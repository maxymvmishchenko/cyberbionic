Using Visual Studio 2010, create a project using the Console Application template.
Create a structure describing a point in a 3D coordinate system. Make it possible to add two points by using the + operator overload.

Solution:

+ Created a structure that contains three fields, a constructor and created an overload of the + operator, which returns a new Point.
+ In the Main method, I created two instances of points and passed the values ​​in the constructors.
+ Add values from first and second points and displayed the result of the operator overload on the screen.