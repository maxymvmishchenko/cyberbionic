﻿using System;
using System.Security.Permissions;

namespace BlockApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Block block1 = new Block(10, 20, 30, 40);
            Block block2 = new Block(30, 40, 20, 10);

            Console.WriteLine(block1.ToString());
            Console.WriteLine(block2.ToString());

            Console.WriteLine(new string('-', 50));

            Console.WriteLine("block1 == block2: {0}", block1.Equals(block2));

            block1 = block2;

            Console.WriteLine("block1 == block2: {0}", block1.Equals(block2));

            Console.WriteLine(new string ('-', 50));

            Console.WriteLine(block1.GetHashCode());
            Console.WriteLine(block2.GetHashCode());

            Console.ReadKey();
        }
    }
}
