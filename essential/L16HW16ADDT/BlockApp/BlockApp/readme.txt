Task 2
Create a Block class with 4 side fields, override the methods in it:
Equals - able to compare blocks with each other,

Solution:

 + Created the Block class. Created 4 fields in the class body;
 + Created the Block constructor and initialized the fields with custom values;
 + Reimplemented the Equals, GetHashCode and ToString method;
 + In the body of the Program method, in the Main method, created an instance of the Block class, set custom values ​​through the constructor;
 + On an instance of the class, addressed the members of the Block class and compared them using the Equals method;
 + Displayed the result of the comparison on the screen.
