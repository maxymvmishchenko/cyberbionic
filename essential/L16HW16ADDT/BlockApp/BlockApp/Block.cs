﻿namespace BlockApp
{
    class Block
    {
        protected int _a,_b,_c,_d;

        public Block(int sideA, int sideB, int sideC, int sideD)
        {
            _a = sideA;
            _b = sideB;
            _c = sideC;
            _d = sideD;

        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
        
            Block p = (Block)obj;
            return (_a == p._a) && (_b == p._b) && (_c == p._c) && (_d == p._d);
        }
        
        public override int GetHashCode()
        {
            return _a ^ _b ^ _c ^ _d;
        }

        public override string ToString()
        {
            return string.Format($"Sides information: A-{_a}, B-{_b}, C-{_c}, D-{_d}");
        }
    }
}