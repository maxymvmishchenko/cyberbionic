The Task
Using Visual Studio, create a project using the Console Application template.
Create class MyList <T>. Implement, in the simplest approximation, the possibility of using an instance of it similar to an instance of the List <T> class. The minimum required interface for interacting with an instance should include a method to add an item, an indexer to get the value of an item at a specified index, and a read-only property to get the total number of items.

Solution:

 - create interface IMyList;
 - in the body we create the signatures of the add item method, the indexer to get the item value at the specified index, and a read-only property to get the total number of items;
 - create class MyList and implement the interface IMyList;
 - in class Program create an instance of the class MyList;
 - add elements to the array, display the array elements on the screen, display the total number of elements

