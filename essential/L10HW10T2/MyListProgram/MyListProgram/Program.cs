﻿using System;

namespace MyListProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            var instance = new MyList<int>();
            instance.Add(101);
            instance.Add(201);
            instance.Add(301);

            Console.WriteLine(instance.Count);
            Console.WriteLine(instance[2]);

            Console.ReadKey();
        }
    }
}
