﻿using System;
using System.Security.Cryptography;
using ClassRoomProgram;

namespace ClassRoomProgram
{
    class Program
    {
        static void Main(string[] args)
        {

            ClassRoom group = new ClassRoom(new ExelentPupil(), new GoodPupil(), new BadPupil());
            
            group.FindRead();
            group.FindRelax();
            group.FindStudy();
            group.FindWrite();
           
            Console.ReadKey();
        }
        
    }
}
