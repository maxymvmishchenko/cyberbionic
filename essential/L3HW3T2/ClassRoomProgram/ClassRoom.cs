﻿using System;
using System.Runtime.CompilerServices;

namespace ClassRoomProgram
{
    class ClassRoom
    {
        private Pupil[] arrPupil;

        public ClassRoom(Pupil p1, Pupil p2, Pupil p3)
        {
            arrPupil = new Pupil[]{p1, p2, p3};
        }

        public void FindStudy()
        {
            Console.WriteLine("FindStudy");

            foreach (Pupil item in arrPupil)
            {
                item.Study();
            }
        }

        public void FindRead()
        {
            Console.WriteLine("FindRead");

            foreach (Pupil item in arrPupil)
            {
                item.Read();
            }
        }

        public void FindWrite()
        {
            Console.WriteLine("FindWrite");

            foreach (Pupil item in arrPupil)
            {
                item.Write();
            }
        }
        public void FindRelax()
        {
            Console.WriteLine("FindRelax");

            foreach (Pupil item in arrPupil)
            {
                item.Relax();
            }
        }

    }
}
