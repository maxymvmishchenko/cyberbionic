﻿using System;

namespace ClassRoomProgram
{
    class Pupil
    {
        public virtual void Study()
        {
        }
        public virtual void Read()
        {
        }
        public virtual void Write()
        {
        }
        public virtual void Relax()
        {
        }
    }

    class ExelentPupil : Pupil
    {
        public override void Study()
        {
            Console.WriteLine("It's an exelent pupil");
        }
        public override void Read()
        {
            Console.WriteLine("It's an exelent pupil");
        }
        public override void Write()
        {
            Console.WriteLine("It's an exelent pupil");
        }
        public override void Relax()
        {
            Console.WriteLine("It's an exelent pupil");
        }
    }

    class GoodPupil : Pupil
    {
        public override void Study()
        {
            Console.WriteLine("It's a good pupil");
        }
        public override void Read()
        {
            Console.WriteLine("It's a good pupil");
        }
        public override void Write()
        {
            Console.WriteLine("It's a good pupil");
        }
        public override void Relax()
        {
            Console.WriteLine("It's a good pupil");
        }

    }
    class BadPupil : Pupil
    {
        public override void Study()
        {
            Console.WriteLine("It's a bad pupil");
        }
        public override void Read()
        {
            Console.WriteLine("It's a bad pupil");
        }
        public override void Write()
        {
            Console.WriteLine("It's a bad pupil");
        }
        public override void Relax()
        {
            Console.WriteLine("It's a bad pupil");
        }
    }

}
