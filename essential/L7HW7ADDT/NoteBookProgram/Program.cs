﻿using System;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace NoteBookProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            NoteBook instance = new NoteBook();
            instance.Constructor("CS-305", "Toshiba", 990.99);
            
            instance.Show();

            Console.ReadKey();
        }
    }
}
