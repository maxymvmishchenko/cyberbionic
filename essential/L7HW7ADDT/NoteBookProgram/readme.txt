The task
Using Visual Studio, create a project using the Console Application template.
Create a structure named Notebook.
Structure fields: model, manufacturer, price.
The structure must implement a constructor for initializing the fields and a method for displaying the contents of the fields on the screen.

Solution:

1. Create a structure in a separate file - struct Notebook.
2. Create fields in the body of the structure:
 - model
 - producer
 - price
3. We create a custom constructor and initialize the fields.
4. Create a Show method in the structure.
5. In the Program file, we display the contents of the fields on the screen.