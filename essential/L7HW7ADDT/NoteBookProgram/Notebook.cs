﻿using System;

namespace NoteBookProgram
{
    public struct NoteBook
    {
        private string _model;
        private string _manufacturer;
        private double _price;

        public string Model
        {
            get { return _model; }
        }

        public string Manufacturer
        {
            get { return _manufacturer; }
        }

        public double Price
        {
            get { return _price; }
        }

        public void Constructor(string model, string manufacturer, double price)
        {
            _manufacturer = manufacturer;
            _model = model;
            _price = price;
        }

        public void Show()
        {
            Console.WriteLine("Model {0} from {1} cost - {2}", _model, _manufacturer, _price);
        }
    }
}
