Task2
Using Visual Studio, create a project using the Console Application template.
Create a collection MyList <T>. Implement, in the simplest approximation, the possibility of using an instance of it in the same way as an instance of the List <T> class. The minimum required interface for interacting with an instance should include a method to add an item, an indexer to get the value of an item at a specified index, and a read-only property to get the total number of items. Implement the ability to iterate over the elements of a collection in a foreach loop.

Solution:
