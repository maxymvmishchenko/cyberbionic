Task 2

Using Visual Studio, create a project using the Console Application template.
Required:
Create a Converter class.
In the body of the class, create a custom constructor that takes three real arguments, and initializes the fields corresponding to the rate of the 3 main currencies, in relation to the hryvnia - public Converter (double usd, double eur, double rub).
Write a program that will convert from the hryvnia into one of the specified currencies, and the program must also convert from the specified currencies to the hryvnia.