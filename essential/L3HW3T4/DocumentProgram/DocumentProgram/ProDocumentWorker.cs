﻿using System;

namespace DocumentProgram
{
    class ProDocumentWorker : DocumentWorker
    {
        public override void EditDocument()
        {
            Console.WriteLine("Document edited in old format");
        }
        public override void SaveDocument()
        {
            Console.WriteLine("Saving in other formats is available in the Expert version");
        }
    }
}
