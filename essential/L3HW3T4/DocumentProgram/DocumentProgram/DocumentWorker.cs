﻿using System;
using System.Runtime.InteropServices;

namespace DocumentProgram
{
    class DocumentWorker
    {

        public void  OpenDocument()
        {
            Console.WriteLine("Open document");
        }
        public virtual void EditDocument()
        {
            Console.WriteLine("Edit document");
        }
        public virtual void SaveDocument()
        {
            Console.WriteLine("Save document");
        }
    }
}
