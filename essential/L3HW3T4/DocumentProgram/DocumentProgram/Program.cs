﻿using System;

namespace DocumentProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input your license-key: ");
            string key = Console.ReadLine();
            DocumentWorker version = null;

            switch (key)
            {
                case "pro":
                {
                    version = new ProDocumentWorker();
                    break;
                }
                case "expert":
                {
                    version = new ExpertDocumentWorker();
                    break;
                }
                default:
                {
                    version = new DocumentWorker();
                    break;
                }
            }

            version.OpenDocument();
            version.EditDocument();
            version.SaveDocument();

            Console.ReadKey();

        }
    }
}
