Task 4
Using Visual Studio, create a project using the Console Application template.
Required:
Create a DocumentWorker class.
In the body of the class, create three methods OpenDocument (), EditDocument (), SaveDocument ().
In the body of each of the methods add the output to the screen of the corresponding lines: "Document is open", "Document editing is available in the Pro version", "Document saving is available in the Pro version".
Derived the ProDocumentWorker class.
Override the appropriate methods, when overriding the methods, output the following lines: "The document has been edited", "The document is saved in the old format, saving in other formats is available in the Expert version".
Derive the ExpertDocumentWorker class from the ProDocumentWorker base class. Override the appropriate method. When you call this method, you must display "Document saved in new format".
In the body of the Main () method, implement the ability to receive the access key number pro and exp from the user. If the user does not enter the key, he can only use the free version (an instance of the base class is created), if the user entered the access key numbers pro and exp, then an instance of the corresponding version of the class, reduced to the base - DocumentWorker, must be created.

Solution:

+ Created a base DocumentWorker class with three methods OpenDocument (), EditDocument (), SaveDocument () and a derived class ProDocumentWorker, in which overridden the methods EditDocument (), SaveDocument ();

+ Derived the ExpertDocumentWorker class and inherited from the ProDocumentWorker base class, where overridden the SaveDocument () method;

+ In the Program class created a SWITCH, let the user enter the "pro" or "expert" key. Created an instance of the corresponding class based on the input of the license key. If the key is entered incorrectly, I let the user work in the free version;

+ Call all methods on the class instance. 

