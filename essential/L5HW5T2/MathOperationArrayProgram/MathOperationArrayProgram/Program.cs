﻿using System;

namespace MathOperationArrayProgram
{
    class Program
    {
        static void Main()
        {

            ArrayArithmeticOperations array = new ArrayArithmeticOperations(10);

            Console.WriteLine(new string ('-', 50));

            array.MinMaxValue();
            Console.WriteLine(new string('-', 50));

            array.SumValue();
            Console.WriteLine(new string('-', 50));

            array.AverageValue();
            Console.WriteLine(new string('-', 50));

            array.OddValue();
            Console.WriteLine(new string('-', 50));


            Console.ReadKey();
        }
    }
}
