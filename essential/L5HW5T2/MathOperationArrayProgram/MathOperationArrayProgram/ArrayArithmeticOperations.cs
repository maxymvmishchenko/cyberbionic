﻿using System;

namespace MathOperationArrayProgram
{
    class ArrayArithmeticOperations
    {
        private readonly int[] _array;

        //user ctor
        public ArrayArithmeticOperations(int elements)
        {
            _array = new int[elements];
            Random random = new Random();
            for (int i = 0; i < _array.Length; i++)
            {
                _array[i] = random.Next(0, 50);
            }
        }

        //MinMaxValue Method
        public void MinMaxValue()
        {
            int min = _array[0];
            int max = _array[0];
            for (int i = 0; i < _array.Length; i++)
            {
                min = Math.Min(min, _array[i]);
                max = Math.Max(max, _array[i]);
            }

            Console.WriteLine("Min element value - {0}, Max element value - {1}", min, max);
        }

        //SumValue Method
        public void SumValue()
        {
            int sum = 0;
            for (int i = 0; i < _array.Length; i++)
            {
                sum += _array[i];
            }

            Console.WriteLine("Sum of elements our array - {0}", sum);
        }
        //AverageValue Method
        public void AverageValue()
        {
            int sum = 0;
            {
                for (int i = 0; i < _array.Length; i++)
                {
                    sum += _array[i];
                }

                Console.WriteLine("Average of array elements - {0}", sum / _array.Length );
            }
        }

        //OddValue Method
        public void OddValue()
        {
            for (int i = 0; i < _array.Length; i++)
            {
                if (_array[i] % 2 != 0)
                {
                    Console.WriteLine("Odd array numbers - {0}", _array[i]);
                }
            }
        }


    }
}
