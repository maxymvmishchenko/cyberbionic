Task 2
Using Visual Studio, create a project using the Console Application template.
Required:
Create an array of N elements, fill it with arbitrary integer values.
Print the largest value of the array, the smallest value of the array, the total sum of the elements, the arithmetic mean of all the elements, print all odd values.


Solution:

 + Create a class ArrayArithmeticOperations, add an array field. Using a custom constructor, we initialize an array field and fill it with derived numbers.

 + For each operation, create methods that are responsible for mathematical operations on array elements - MinMaxValue, SumValue, AverageValue, OddValue in the body of which we display the value on the screen.

+ In the Program class, create an instance of the ArrayArithmeticOperations class. We call our methods on an instance of the class.

