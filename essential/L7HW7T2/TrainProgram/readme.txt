The task 2
Using Visual Studio, create a project using the Console Application template.
Required: Describe a structure named Train containing the following fields: destination name, train number, departure time.
Write a program that does the following:
- entering data from the keyboard into an array consisting of eight elements of the Train type (the records must be ordered by train numbers);
- displaying information about the train, the number of which was entered from the keyboard (if there are no such trains, display a corresponding message).