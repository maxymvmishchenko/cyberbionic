The task
We use Visual Studio, we create the project according to the template of the console program.
In the collection ArrayList, through the method of select Add add elements of structural and reference type, select this collection using, loop for - With what problem call?

Solution:

 + Create an instance of the class ArrayList <int> and close with type int. We pass the reference to an instance of a class to the implicitly typed variable array.
 + On an instance of the array class, call the Add method and place our element in the collection.
 + Using the for loop, display the values ​​of the elements in our collection.

Errot: ArrayList is a heterogeneous collection. UnBoxing is not possible when adding reference and structure types to the ArrayList. Exception: System.InvalidCastException: 'Specified cast is not valid.'