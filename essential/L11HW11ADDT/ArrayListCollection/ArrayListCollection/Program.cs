﻿using System;
using System.Collections;

namespace ArrayListCollection
{
    class Program
    {
        static void Main()
        {
            ArrayList array = new ArrayList();

            array.Add("Hello");
            array.Add("World");
            array.Add(10);
            array.Add(20);

            for (int i = 0; i < array.Count; i++)
                Console.WriteLine((int)array[i]);

            Console.WriteLine(new string('-', 50));
            Console.ReadKey();
        }
    }
}
