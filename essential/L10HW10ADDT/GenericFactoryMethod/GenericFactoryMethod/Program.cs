﻿using System;
using GenericFactoryMethod;

namespace GenericFactoryMethod
{
    public class MyClass<T> where T : new()
    {
    public T FactoryMethod()
    {
        return new T();
    }
    }
}

class Program
{
    static void Main(string[] args)
    {
        var instance = new MyClass<bool>();
        Console.WriteLine(instance.GetType());
        Console.ReadKey();
    }
}
