Task 2
We use Visual Studio, we create the project according to the template of the console program.
Create a program that will display a string of falling characters. The length of each chain is set randomly. The first symbol of the chain is white, the second symbol is light green, the other symbols are dark green. During the fall of the chain, at each step, all the characters change their meaning. When it reaches the end, the chain disappears and a new chain is formed from above. See the screenshot below as a sample.

Solution:
