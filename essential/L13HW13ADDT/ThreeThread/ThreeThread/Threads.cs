﻿using System;
using System.Threading;

namespace ThreeThread
{
    class Threads
    {

        public void FirstThread()
        {
            while (true)
            {
                Console.WriteLine("First Thread");
                Thread.Sleep(300);
            }
        }

        public void SecondThread()
        {
            while (true)
            {
                Console.WriteLine(new string(' ', 20) + "Second Thread");
                Thread.Sleep(300);
            }
        }

        public void ThirdThread()
        {
            while (true)
            {
                Console.WriteLine(new string(' ', 40) + "Third Thread");
                Thread.Sleep(300);
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }
    }
}