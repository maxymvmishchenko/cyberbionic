﻿using System;
using System.Threading;

namespace ThreeThread
{
    class Program
    {
        static void Main(string[] args)
        {
            Threads instance = new Threads();
            Console.SetWindowSize(85,40);

            for (int counter = 0; counter < 10; counter++)
            {
                new Thread(instance.FirstThread).Start();
                new Thread(instance.SecondThread).Start();
                new Thread(instance.ThirdThread).Start();
            }

            Console.Read();
        }
    }
}
