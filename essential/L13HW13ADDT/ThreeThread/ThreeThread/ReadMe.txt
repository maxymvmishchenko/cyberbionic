Task 1
Using Visual Studio 2010, create a project using the Console Application template.
Write a program that calls the method recursively.
Each new method call is performed on a separate thread.

Solution:

 - Create class MyClass.
 - In the class MyClass create three methods.
 - Each of the three methods outputs a string and is a separate thread.
 - In the class Program in the Main method create instance MyClass.
 - Сreate a loop in it through the Thread class and call each method creating three threads.