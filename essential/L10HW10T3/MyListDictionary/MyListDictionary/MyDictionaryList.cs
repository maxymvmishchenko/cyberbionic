﻿using System.Linq.Expressions;

namespace MyListDictionary
{
    class MyDictionaryList<TKey, TValue>
    {
        private readonly TKey[] _key;
        private readonly TValue[] _value;
        private readonly int _length;

        public MyDictionaryList(int n)
        {
            _key = new TKey[n];
            _value = new TValue[n];
            _length = n;
        }
        public void Add(int i, TKey k, TValue v)
        {
            _key[i] = k;
            _value[i] = v;
        }

        public string this[int index]
        {
            get
            {
                if (index >= 0 && index < _key.Length)
                    return _key[index] + " - " + _value[index];
                return "An attempt was made to access the array.";
            }
        }

        public int Length
        {
            get
            {
                return _length;
            }

        }
    }
}
