Task 3
Using Visual Studio, create a project using the Console Application template.
Create class MyDictionary <TKey, TValue>. Implement, in the simplest approximation, the possibility of using an instance of it in the same way as an instance of the Dictionary class (Lesson 6 example 5). The minimum required interface for interacting with an instance should include a method for adding element pairs, an indexer to get the value of an element at a specified index, and a read-only property to get the total number of element pairs.

Solution: 

- create class MyDictionaryList;
 - in the body we create the signatures of the add item method, the indexer to get the item value at the specified index, and a  property to get the total number of items;
 - in class Program create an instance of the class MyDictionareList;
 - add elements to the array, display the array elements on the screen, display the total number of elements.
