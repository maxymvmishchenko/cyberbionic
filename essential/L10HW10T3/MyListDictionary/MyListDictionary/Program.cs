﻿using System;

namespace MyListDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите размерность словаря:");
            int n = Convert.ToInt32(Console.ReadLine());
            var array = new MyDictionaryList<string, string>(n);

            array.Add(0, "Привет", "Hello");
            array.Add(0, "Лиса", "Fox");


            for (int i = 0; i < n; i++)
            {
                Console.WriteLine(array[i]);
            }

            Console.WriteLine(array.Length);

            Console.ReadKey();
        }
    }
}
