﻿using System;

namespace CarProgram
{
    public interface ITransmission
    {
        void TransChoice();
    }
    public class Manual : ITransmission
    {
        public void TransChoice()
        {
            Console.WriteLine("Your choice Manual transmission");
        }

    }

    public class Auto : ITransmission
    {
        public void TransChoice()
        {
            Console.WriteLine("Your choice Auto transmission");
        }
    }
}
