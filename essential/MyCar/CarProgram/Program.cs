﻿using System;

namespace CarProgram
{
    class Program
    {
        static void Main()
        {
            var gasEngine = new GasEngine();
            var electricEngine = new ElectricEngine();

            var manual = new Manual();
            var auto = new Auto();

            Console.WriteLine("Input: \n1 - GasEngine \n2 - ElectricEngine");
            var select = Int32.Parse(Console.ReadLine());
            IEngine selectEngine = null;

            switch (select)
            {
                case 1:
                    {
                        selectEngine = gasEngine;
                        break;
                    }
                case 2:
                    {
                        selectEngine = electricEngine;
                        break;
                    }
            }

            Console.WriteLine("Input: \n1 - ManualTransmission \n2 - AutoTransmission");
            select = Int32.Parse(Console.ReadLine());
            ITransmission selectTransmission = null;

            switch (select)
            {
                case 1:
                    {
                        selectTransmission = manual;
                        break;
                    }
                case 2:
                    {
                        selectTransmission = auto;
                        break;
                    }
            }

            var car = new Car(selectEngine, selectTransmission);

            Console.WriteLine(car.Engine);
            car.EngineChoice();
            car.TransChoice();
            car.Engine = electricEngine;

            Console.ReadKey();

        }
    }

}