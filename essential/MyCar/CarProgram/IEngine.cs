﻿using System;

namespace CarProgram
{
    public interface IEngine
    {
        void ChoiceEngine();
    }

    public class GasEngine : IEngine
    {
        public void ChoiceEngine()
        {
            Console.WriteLine("Your Choice GasEngine");
        }
    }

    public class ElectricEngine : IEngine
    {
        public void ChoiceEngine()
        {
            Console.WriteLine("Your Choice ElectricEngine");
        }
    }

}
