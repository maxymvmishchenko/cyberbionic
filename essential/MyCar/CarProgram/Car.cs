﻿using System;

namespace CarProgram
{
    public class Car
    {
        private IEngine _engine;
        private ITransmission _transmission;
        
        public Car(IEngine engine, ITransmission transmission)
        {
            _engine = engine;
            _transmission = transmission;
        }

        public IEngine Engine
        {
            get => _engine;
            set => _engine = value;
        }

        public void EngineChoice()
        {
            _engine.ChoiceEngine();
        }

        public void TransChoice()
        {
            _transmission.TransChoice();
        }
    }
}
