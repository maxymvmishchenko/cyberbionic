﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlayerProgram
{
    class Recorder : IRecodable
    {
        public void Record()
        {
            Console.WriteLine("Sound recording method");
        }

        public void Pause()
        {
            Console.WriteLine("Audio recording pause method");
        }

        public void Stop()
        {
            Console.WriteLine("Audio recording stop method");
        }
    }
}
