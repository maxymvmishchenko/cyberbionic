﻿using System;

namespace PlayerProgram
{
    class Player :  IPlayable
    {
        public void Play()
        {
            Console.WriteLine("Song playback method");
        }

        public void Pause()
        {
            Console.WriteLine("Song pause method");
        }

        public void Stop()
        {
            Console.WriteLine("Song stop method");
        }
    }
}
