Task 3
Using Visual Studio, create a project using the Console Application template.
Required:
Create 2 interfaces IPlayable and IRecodable. In each of the interfaces, create 3 methods void Play () / void Pause () / void Stop () and void Record () / void Pause () / void Stop (), respectively.
Derive the Player class from the base IPlayable and IRecodable interfaces.
Write a program that performs playback and recording.

Solution:
+ Create 2 interfaces IPlayable and IRecodable. In each of the interfaces, we will create 3 methods void Play () / void Pause () / void Stop () and void Record () / void Pause () / void Stop (), respectively.
+ About the IPlayable interface, I will inherit the IRecodable interface.
+ Derived the Player class and implement the IPlayable and IRecodable interfaces.
+ In the Program class, create an instance of the IRecodable type and call the void Play () / void Pause () / void Stop () methods
+ I will create an instance of the IPlayable type and call the Record () / void Pause () / void Stop () methods.
