﻿namespace PlayerProgram
{
    interface IPlayable
    {
        void Play();
        void Pause();
        void Stop();
    }
}
