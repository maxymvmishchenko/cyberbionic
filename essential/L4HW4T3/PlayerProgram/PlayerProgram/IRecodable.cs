﻿namespace PlayerProgram
{

    interface IRecodable
    {
        void Record();
        void Pause();
        void Stop();

    }
}
