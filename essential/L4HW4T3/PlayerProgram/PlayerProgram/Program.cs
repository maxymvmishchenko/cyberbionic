﻿using System;

namespace PlayerProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(new string('-', 60));
            Console.WriteLine("Features of our player: ");
            Console.WriteLine(new string('-', 60));

            IPlayable player = new Player();
            player.Play();
            player.Pause();
            player.Stop();


            Console.WriteLine(new string('-', 60));
            Console.WriteLine("Features of our recorder: ");
            Console.WriteLine(new string('-', 60));

            IRecodable record = new Recorder();
            record.Record();
            record.Pause();
            record.Stop();

            Console.ReadKey();
        }
    }
}
