﻿using System;

namespace QuestionnaireProgram
{
    class User
    {
        private string _login, _name, _surname;
        private int _age;

        public string Login
        {
            set { _login = value; }
            get
            {
                if (_login == null)
                    return "The field is not filled";
                return _login;
            }
        }

        public string Name
        {
            set { _name = value; }
            get
            {
                if (_name == null)
                    return "The field is not filled";
                return _name;
            }
        }

        public string Surname
        {
            set { _surname = value; }
            get
            {
                if (_surname == null)
                    return "Поле не заполнено";
                return _surname;
            }
        }

        public int Age
        {
            set { _age = value; }
            get
            {
                if (_age <= 0)
                    return 18;
                return _age;
            }
        }
        
        public User(string login, string name, string familyname, int old)
        {
            login = _login;
            _name = name;
            _surname = familyname;
            _age = old;
            
        }

    }
}
