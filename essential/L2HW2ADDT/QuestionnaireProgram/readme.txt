The add task

Using Visual Studio, create a project using the Console Application template.
Required:
Create a User class containing information about the user (login, first name, last name, age, date of filling out the questionnaire). The field date of filling out the questionnaire must be initialized only once (when creating an instance of this class) without the possibility of further changing it.
Implement display of user information.

Solution:

+ We create the User class. We create three fields in the class:
+ 1 field with the type string login, name, surname.
+ 1 field of type int - age;
+ 1 field with filling time.

+ Using the properties - set, we assign the values ​​entered by the user to the fields. Get the values ​​using get.

+ In the Program class, we create an instance of the User class, and display the data on the screen.