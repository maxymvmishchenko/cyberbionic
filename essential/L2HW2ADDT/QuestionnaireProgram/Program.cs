﻿using System;
using System.Diagnostics.Eventing.Reader;

namespace QuestionnaireProgram
{
    class Program
    {
        static void Main()
        {
            User user = new User();
            Console.WriteLine(user.Surname);

            user.Surname = "Max";
            Console.WriteLine(user.Surname);

            Console.WriteLine(new string('-', 30));

            User user2 = new User("velmin", "Max", "Mischenko", 31);
            Console.WriteLine(user2.Surname);

            Console.ReadKey();
        }
    }
}
