Task 2
Using Visual Studio, create a project using the Console Application template.
Required:
Create AbstractHandler class.
In the body of the class, create methods void Open (), void Create (), void Chenge (), void Save ().
Derive XMLHandler, TXTHandler, DOCHandler classes from AbstractHandler base class.
Write a program that will perform the definition of a document and for each format there should be methods for opening, creating, editing, saving a specific document format.

Solution:

+ Create an abstract base class AbstractHandler in which.
+ In the body of the abstract class, create abstract methods void Open (), void Create (), void Chenge (), void Save ().
+ Create three classes
 XMLHandler, TXTHandler, DOCHandler and override methods from the abstract class in them.
+ In the Program class, create an instance of each derived class and call the Open, void Create, void Chenge, void Save methods on the class instance.
