﻿using System;
using System.Runtime.InteropServices;

namespace DocumentProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Document DOCHandler and operations on them: ");
            DOCHandler doc = new DOCHandler();
            doc.Create();
            doc.Change();
            doc.Open();
            doc.Save();

            Console.WriteLine(new string ('-', 55));
            Console.WriteLine("Document XMLHandler and operations on them: ");

            XMLHandler xml = new XMLHandler();
            xml.Create();
            xml.Change();
            xml.Open();
            xml.Save();

            Console.WriteLine(new string('-', 55));
            Console.WriteLine("Document TXTHandler and operations on them: ");

            TXTHandler txt = new TXTHandler();
            txt.Create();
            txt.Change();
            txt.Open();
            txt.Save();

            Console.WriteLine(new string('-', 55));

            Console.ReadKey();

        }
    }
}
