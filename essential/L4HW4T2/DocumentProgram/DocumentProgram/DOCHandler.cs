﻿using System;

namespace DocumentProgram
{
    class DOCHandler : AbstractHandler
    {
        public override void Open()
        {
            Console.WriteLine("DOCHandler - Open Document");
        }
        public override void Create()
        {
            Console.WriteLine("DOCHandler - Create Document");
        }
        public override void Change()
        {
            Console.WriteLine("DOCHandler - Change Document");
        }

        public override void Save()
        {
            Console.WriteLine("DOCHandler - Save Document");
        }
    }
}
