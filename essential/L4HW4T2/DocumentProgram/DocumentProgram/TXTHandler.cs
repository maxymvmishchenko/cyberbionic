﻿using System;

namespace DocumentProgram
{
    class TXTHandler : AbstractHandler
    {
        public override void Open()
        {
            Console.WriteLine("TXTHandler - Open Document");
        }
        public override void Create()
        {
            Console.WriteLine("TXTHandler - Create Document");
        }
        public override void Change()
        {
            Console.WriteLine("TXTHandler - Change Document");
        }

        public override void Save()
        {
            Console.WriteLine("TXTHandler - Save Document");
        }
    }
}
