﻿using System;

namespace DocumentProgram
{
    class XMLHandler: AbstractHandler
    {
        public override void Open()
        {
            Console.WriteLine("XMLHandler - Open Document");
        }
        public override void Create()
        {
            Console.WriteLine("XMLHandler - Create Document");
        }
        public override void Change()
        {
            Console.WriteLine("XMLHandler - Change Document");
        }

        public override void Save()
        {
            Console.WriteLine("XMLHandler - Save Document");
        }
}
}
