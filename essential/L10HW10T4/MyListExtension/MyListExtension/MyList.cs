﻿namespace MyListExtension
{
    public class MyList<T>
    {

        private T[] _array;

        public MyList()
        {
            _array = new T[0];
        }

        public T this[int index] => _array[index];

        public int Count => _array.Length;

        public void Add(T a)
        {
            T[] currentArray = new T[_array.Length + 1];
            for (int i = 0; i < _array.Length; i++)
            {
                currentArray[i] = _array[i];
            }

            currentArray[_array.Length] = a;
            _array = currentArray;
        }
    }
}
