Task 4
Using Visual Studio, create a project using the Console Application template.
Create an extension method: public static T [] GetArray <T> (this MyList <T> list)
Apply an extension method to the instance of type MyList <T> developed in Homework 2 for this lesson. Display the values ​​of the elements of the array returned by the GetArray () extension method.

Solution:
