﻿using System;
using PartDocumentProgram;

namespace PartDocumentProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            Document document = new Document("Contract");
            document.Body = "Some Body";
            document.Footer = "Some footer";

            document.Show();

            Console.ReadKey();
        }
    }
}
