﻿using System.Runtime.InteropServices;

namespace PartDocumentProgram
{
    class Document
    {
        private Title _title = null;
        private Body _body = null;
        private Footer _footer = null;

        void Initialize()
        {
            _title = new Title();
            _body = new Body();
            _footer = new Footer();
        }

        public Document(string title)
        {
            Initialize();
            _title.Content = title;
        }

        public void Show()
        {
            _title.Show();
            _body.Show();
            _footer.Show();
        }

        public string Body
        {
            set
            {
                _body.Content = value;
            }
        }

        public string Footer
        {
            set
            {
                _footer.Content = value;
            }
        }

    }
}
