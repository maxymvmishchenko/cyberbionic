﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartDocumentProgram
{
    class Body : PartDocument
    {
        public override string Content
        {
            get
            {
                if (_content != null)
                    return _content;
                else
                    return "Body is empty";
            }
            set
            {
                _content = value;
            }
        }

        public override void Show()
        {
            Console.WriteLine(Content);
        }

    }
}
