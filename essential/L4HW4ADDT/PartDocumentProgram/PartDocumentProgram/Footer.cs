﻿using System;

namespace PartDocumentProgram
{
    class Footer : PartDocument
    {
        public override string Content
        {
            get
            {
                if (_content != null)
                    return _content;
                else
                    return "Footer is empty";
            }
            set
            {
                _content = value;
            }
        }

        public override void Show()
        {
            Console.WriteLine(Content);
        }
    }
}
