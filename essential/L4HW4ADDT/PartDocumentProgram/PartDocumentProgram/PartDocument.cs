﻿namespace PartDocumentProgram
{
    abstract class PartDocument
    {
        protected string _content;
        public abstract string Content { get; set; }
        public abstract void Show();
    }
}
