﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PartDocumentProgram
{
    class Title : PartDocument
    {
        public override string Content
        {
            get
            {
                    return _content;
            }
            set
            {
                _content = value;
            }
        }

        public override void Show()
        {
            Console.WriteLine(Content);
        }
    }
}
