Secong Task

Using Visual Studio, create a project using the Console Application template.
Required: Create a class named Rectangle.
In the body of the class, create two fields that describe the lengths of the sides double side1, side2.
Create a custom constructor Rectangle (double side1, double side2), in the body of which the fields side1 and side2 are initialized with the values ​​of the arguments.
Create two methods that calculate the area of ​​the rectangle - double AreaCalculator () and the perimeter of the rectangle - double PerimeterCalculator ().
Create two properties double Area and double Perimeter with one get accessor.
Write a program that takes the lengths of two sides of a rectangle from the user and displays the perimeter and area.

The Solution:

+Create a file named Rectangle.cs that will contain the functionality.
+In the Rectangle file, create a class named Rectangle, in its body we create two private fields that describe the lengths of the sides double side1, side2.
+Create a custom constructor Rectangle (double side1, double side2), in the body of which the fields side1 and side2 are initialized with the values ​​of the arguments.
+Сreate two methods that calculate the area of ​​the rectangle - double AreaCalculator () and the perimeter of the rectangle - double PerimeterCalculator ().
+We create two properties double Area and double Perimeter with one get accessor.

+In the Program.cs file, in the Main method, we take the lengths of the two sides of the rectangle and convert them to the Double data type.
+We create an instance of the class.
+We display the value of the perimeter and area on the screen.