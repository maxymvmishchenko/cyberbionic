﻿namespace CreateClassRectangleProgram
{
    class Rectangle
    {
        double side1, side2;

        /// <summary>
        /// Построение прямоугольника.
        /// </summary>
        /// <param name="side1">Длина первой стороны</param>
        /// <param name="side2">Длина второй стороны</param>
        public Rectangle(double side1, double side2)
        {
            this.side1 = side1;
            this.side2 = side2;
        }

        /// <summary>
        /// Метод Расчета периметра.
        /// </summary>
        /// <returns>Периметр</returns>
        /// 
        double PerimeterCalculator()
        {
            return (side1 + side2) * 2;
        }
        //создаение свойства
        public double Perimeter
        {
            get
            {
                return this.PerimeterCalculator();
            }
        }

        /// <summary>
        /// МЕтод Расчета площади
        /// </summary>
        /// <returns>Площадь</returns>
        /// 
        double AreaCalculator()
        {
            return side1 * side2;
        }
        //создаение свойства
        public double Area
        {
            get
            {
                return this.AreaCalculator();
            }
        }
    }
}