﻿using System;

namespace CreateClassRectangleProgram
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Input first side Rectangle: ");
            double side1 = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Input second side Rectangle: ");
            double side2 = Convert.ToDouble(Console.ReadLine());

            Rectangle rectangle = new Rectangle(side1, side2);
            Console.WriteLine("Perimeter = {0}, Area= {1}", rectangle.Perimeter, rectangle.Area);

            // Delay.
            Console.ReadKey();
        }
    }
}