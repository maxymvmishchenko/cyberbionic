Additional task
Expand example 5 (Russian-English dictionary) also with the Ukrainian dictionary. Implement the ability to search not only by key Russian words and words in other languages.

Solution:

+ Let's create a Dictionary class. In the body of the class, we will create three private fields for each dictionary in an array.

+ Using the default constructor, I will initialize the array fields with some values.

+ Using three indexed properties and a for loop, I'll go through our filled arrays.

+ Using the if-else construction, I will check if the word is in our dictionary. If there is a translation of a word, I will assign a concatenation of words in Ukrainian, Russian and English. If there is no word in our dictionary, we will display the corresponding message.

+ In the body of the Program class, create an instance of the Dictionary class. On an instance of the class, using the for loop, we display the values ​​of each element of the array on the screen.