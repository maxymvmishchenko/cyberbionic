﻿namespace DictionaryProgram
{
    class Dictionary
    {
        private string[] key = new string[5];
        private string[] rus = new string[5];
        private string[] eng = new string[5];

        public Dictionary()
        {
            key[0] = "стіл";   rus[0] = "стол";   eng[0] = "table";
            key[1] = "яблуко"; rus[1] = "яблоко"; eng[1] = "apple";
            key[2] = "стул";   rus[2] = "стул";   eng[2] = "chair";
            key[3] = "обід";   rus[3] = "обед";   eng[3] = "dinner";
            key[4] = "біг";    rus[4] = "бег";    eng[4] = "run";

        }

        public string this[int index]
        {
            get
            {
                if (index >= 0 && index < key.Length)
                    return key[index] + " - " + rus[index] + " - " + eng[index];
                return "An attempt was made to access the array.";
            }
        }

        public string this[string index]
        {
            get
            {
                for (int i = 0; i < key.Length; i++)
                {
                    if (key[i] == index)
                        return key[i] + "-" + rus[i] + "-" + eng[i];
                    if (rus[i] == index)
                        return key[i] + "-" + rus[i] + "-" + eng[i];
                    if (eng[i] == index)
                        return key[i] + "-" + rus[i] + "-" + eng[i];
                }
                return string.Format("{0} There is no such word in the dictionary", index);

            }
        }
    }
}
