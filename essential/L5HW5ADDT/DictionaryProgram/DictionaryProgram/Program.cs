﻿using System;

namespace DictionaryProgram
{
    class Program
    {
        static void Main()
        {
            Dictionary dictionary = new Dictionary();

            Console.WriteLine(dictionary["стіл"]);
            Console.WriteLine(dictionary["яблуко"]);
            Console.WriteLine(dictionary["стул"]);
            Console.WriteLine(dictionary["обід"]);
            Console.WriteLine(dictionary["виделка"]);
            Console.WriteLine(dictionary["груша"]);
            Console.WriteLine(dictionary["абрикос"]);
            Console.WriteLine(dictionary["біг"]);

            Console.WriteLine(new string ('-', 60));


            for (int i = 0; i < 6; i++)
            {
                Console.WriteLine(dictionary[i]);
            }


            Console.ReadKey();
        }
    }
}
