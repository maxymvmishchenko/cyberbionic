﻿namespace CarPark
{
    public class Car
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
    public class ConsoleCar : Car
    {
        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, {nameof(Age)}: {Age}";
        }
    }
}