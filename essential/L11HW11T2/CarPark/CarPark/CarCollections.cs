﻿namespace CarPark
{
    public class CarCollections : MyList<Car>
    {
        public void AddCar(string name, int age)
        {
            Add(new Car {Age = age, Name = name});
        }

        public void AddConsoleCar(string name, int age)
        {
            Add(new ConsoleCar {Age = age, Name = name});
        }
    }
}