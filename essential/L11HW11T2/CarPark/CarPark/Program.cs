﻿using System;

namespace CarPark
{
    class Program
    {
        static void Main(string[] args)
        {
            var carName = new CarCollections();
            carName.AddConsoleCar("Audi", 2002);
            carName.AddConsoleCar("Porche", 2005);
            carName.AddConsoleCar("Lada", 1998);

            foreach (var i in carName)
            {
                Console.WriteLine(i);
            }

            
            Console.ReadKey();


        }
    }
}
