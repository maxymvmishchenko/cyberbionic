﻿namespace EventCalculator
{
    class Model
    {
        public string AddMethod(int a, int b)
        {
            return (a + b).ToString();
        }
        public string SubMethod(int a, int b)
        {
            return (a - b).ToString();
        }
        public string MulMethod(int a, int b)
        {
            return (a * b).ToString();
        }
        public string DevMethod(int a, int b)
        {
            return ((double)a / b).ToString();
        }
    }
}
