﻿using System;
using System.Windows;

namespace EventCalculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            new Presenter(this);
        }

        private event EventHandler addEvent = null;
        private event EventHandler subEvent = null;
        private event EventHandler mulEvent = null;
        private event EventHandler devEvent = null;

        public event EventHandler AddEvent
        {
            add { addEvent += value;}
            remove { addEvent -= value; }
        }

        public event EventHandler SubEvent
        {
            add { subEvent += value;}
            remove { subEvent -= value; }
        }

        public event EventHandler MulEvent
        {
            add { mulEvent += value; }
            remove { mulEvent -= value; }
        }
        public event EventHandler DevEvent
        {
            add { devEvent += value; }
            remove { devEvent -= value; }
        }

        public void AddButton(object sender, RoutedEventArgs e)
        {
            addEvent.Invoke(sender, e);
        }

        public void SubButton(object sender, RoutedEventArgs e)
        {
            subEvent.Invoke(sender, e);
        }

        public void MulButton(object sender, RoutedEventArgs e)
        {
            mulEvent.Invoke(sender, e);
        }

        public void DevButton(object sender, RoutedEventArgs e)
        {
            devEvent.Invoke(sender, e);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
