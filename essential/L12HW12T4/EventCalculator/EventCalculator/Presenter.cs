﻿using System;

namespace EventCalculator
{
    class Presenter
    {
        private Model model;
        private MainWindow mainWindow;

        public Presenter(MainWindow mainWindow)
        {
            model = new Model();
            this.mainWindow = mainWindow;
            this.mainWindow.AddEvent += MainWindow_AddEvent;
            this.mainWindow.SubEvent += MainWindow_SubEvent;
            this.mainWindow.MulEvent += MainWindow_MulEvent;
            this.mainWindow.DevEvent += MainWindow_DevEvent;
        }

        private void MainWindow_DevEvent(object sender, EventArgs e)
        {
            string result = model.DevMethod(Convert.ToInt32(mainWindow.Variable1.Text),
                Convert.ToInt32(mainWindow.Variable2.Text));
            mainWindow.Result.Text = result;
        }

        private void MainWindow_MulEvent(object sender, EventArgs e)
        {
            string result = model.MulMethod(Convert.ToInt32(mainWindow.Variable1.Text),
                Convert.ToInt32(mainWindow.Variable2.Text));
            mainWindow.Result.Text = result;
        }

        private void MainWindow_SubEvent(object sender, EventArgs e)
        {
            string result = model.SubMethod(Convert.ToInt32(mainWindow.Variable1.Text),
                Convert.ToInt32(mainWindow.Variable2.Text));
            mainWindow.Result.Text = result;
        }

        private void MainWindow_AddEvent(object sender, EventArgs e)
        {
            string result = model.AddMethod(Convert.ToInt32(mainWindow.Variable1.Text),
                Convert.ToInt32(mainWindow.Variable2.Text));
            mainWindow.Result.Text = result;
        }
    }
}
