﻿using System;

namespace PrintProgram
{
    class Program
    {
        static void Main()
        {
            ColorPrinter printRed = new ColorPrinter(ConsoleColor.Red);
            printRed.Print("Red Color");

            Printer printUp = printRed;
            printUp.Print("Red Color");
            Console.WriteLine(new string ('-', 50));

            ColorPrinter printGreen = new ColorPrinter(ConsoleColor.Green);
            printGreen.Print("Green Color");

            ColorPrinter printBlue = new ColorPrinter(ConsoleColor.Blue);
            printBlue.Print("Hello");

            Console.ReadKey();
        }
    }
}
