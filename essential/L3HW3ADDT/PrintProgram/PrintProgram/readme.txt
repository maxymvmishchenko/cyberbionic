The task
Using Visual Studio, create a project using the Console Application template.
Required:
Create a Printer class.
In the class body, create a void Print (string value) method that prints the value of the argument.
Implement the possibility that if other classes are inherited from this class and the corresponding method of their instance is called, the strings passed as method arguments are displayed in different colors.
Be sure to use typecasting.

Solution:

+ Create a base class Printer. A field, a custom constructor and a virtual Print method were created in the class. As the value of the _color field, we pass the Color Enumeration type. In the virtual method Print, we pass the value of the argument and display it on the screen.

+ Create a derived class ColorPrinter that inherits from the base Printer class. In the body of the class, create a default constructor that will initialize the field with the user value.

+ In the Program class, create an instance of the ColorPrinter class, pass the color as an argument in the constructor. On an instance of the derived ColorPrinter class, we call a method from the base class and pass the argument value.

+ UpCast to base type Printer.

