﻿using System;

namespace PrintProgram
{
    class ColorPrinter : Printer
    {
        public ColorPrinter(ConsoleColor color)
            : base(color)
        {
        }
    }
}
