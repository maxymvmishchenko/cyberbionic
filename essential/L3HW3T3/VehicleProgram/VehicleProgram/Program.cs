﻿using System;
using System.Security.Cryptography;

namespace VehicleProgram
{
    class Program
    {
        static void Main(string[] args)
        {

            Plane plane = new Plane(20,30, 1000000, 950,2021);
            plane.Plane(150, 1000);

            Ship ship = new Ship(20, 30, 1000000, 950, 2021);
            ship.Ship(250, "America");

            Car car = new Car(20, 30, 1000000, 950, 2021);
            car.Car();

            Console.ReadLine();
        }
    }
}
