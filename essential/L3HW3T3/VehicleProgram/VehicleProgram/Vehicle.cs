﻿using System;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

namespace VehicleProgram
{
    class Vehicle
    {
        protected int _x;
        protected int _y;
        protected int _price;
        protected int _speed;
        protected int _year;

        public Vehicle(int x, int y, int price, int speed, int year)
        {
            _x = x;
            _y = y;
            _price = price;
            _speed = speed;
            _year = year;
        }

        public void Plane(int planePassenger, int height)
        {
            Console.WriteLine("Пассажиров в самолете - {0}, Высота - {1}, назначение - {2}, высота - {3}, цена - {4}, скорость - {5}, год - {6}", planePassenger, height, _x, _y, _price, _year,_speed);
        }

        public void Ship(int shipPassenger, string destination)
        {
            Console.WriteLine("Пассажиров на корабле - {0}, назначение - {1}, высота - {2}, цена - {3}, скорость - {4}, год - {5}", shipPassenger, destination, _x, _y, _price, _year, _speed);
        }

        public void Car()
        {
            Console.WriteLine("Назначение - {0}, высота - {1}, цена - {2}, скорость - {3}, год - {4}", _x, _y, _price, _year, _speed);
        }
    }
}
