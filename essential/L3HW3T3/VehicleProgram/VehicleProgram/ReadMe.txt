Task 3
Using Visual Studio, create a project using the Console Application template.
Required:
Create Vehicle class.
In the body of the class, create fields: coordinates and parameters of vehicles (price, speed, year of manufacture).
Create 3 derived classes Plane, Sag and Ship.
For Plane class, the height and number of passengers must be determined.
For the Ship class, the number of passengers and the home port.
Write a program that displays information about each vehicle.

Solution:

+ Created a base Vehicle class, in which I created 5 fields coordinates and parameters of vehicles, price, speed, year of manufacture. Created 3 methods Car, Ship, Plane in the class.

+ Created derived classes Ship, Plane, Car, which inherit from the base Vehicle class. In each of them, I created a constructor that initializes the fields of the Vehicle class.

+ In the Program class, instantiated each derived class. On the instance of the class, I called the methods from the base class Vehicle in the constructor, entered the values ​​of the fields, passed the arguments of height, number of passengers, port of registration to the methods.


