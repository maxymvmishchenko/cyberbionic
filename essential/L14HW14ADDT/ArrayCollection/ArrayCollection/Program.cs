﻿using System;

namespace ArrayCollection
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            Model.GetArrayCollection(array);

            foreach (var arr in array)
            {
                if (arr % 2 == 0)
                {
                    Console.WriteLine(arr);
                }
            }

            Console.Read();
        }
    }
}
