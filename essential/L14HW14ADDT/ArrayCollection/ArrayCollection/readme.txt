The task
Using Visual Studio, create a project using the Console Application template.
Create a method that takes an array of integers as an argument and returns a collection of all even numbers in the array. Vicory the yield statement to form the collection.

Solution:

 - create a Model class in which we create a static method GetArrayCollection of type IEnumerable, which using the yield keyword will return a collection of array elements.
 - in Program, we create an array of integer elements. On the object class, call GetArrayCollection and pass our array of integer elements as a parameter.
 - using a foreach loop, we iterate over our collection.
 - In the body of the loop with foreach, using if, we check and return the collection of even numbers of the array and display it.