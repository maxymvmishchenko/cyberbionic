﻿using System;

namespace AddItemBookProgram
{
    //create class Title
    public class Title
    {
        private readonly string _title;

        public Title(string title)
        {
            _title = title;
        }

        public void Show()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Title: {0}", _title);
        }

    }
}
