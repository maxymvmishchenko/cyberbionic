﻿using System;

namespace AddItemBookProgram
{
    //create class Author
    public class Author
    {
        private readonly string _author;

        public Author(string author)
        {
            _author = author;
        }

        public void Show()
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Author: {0}", _author);
        }
    }
}
