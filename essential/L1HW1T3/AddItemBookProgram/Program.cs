﻿using System;
using System.Configuration;

namespace AddItemBookProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            string authorName = "Max";
            var author = new Author(authorName);

            string titleContent = "Some title";
            var title = new Title(titleContent);

            string contentText = "Some content";
            Content content = new Content(contentText);

            Book book = new Book(author, title, content);

            book.Show();

            // Delay.
            Console.ReadKey();
        }
    }
}
