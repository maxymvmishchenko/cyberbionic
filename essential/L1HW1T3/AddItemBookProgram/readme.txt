The Task 3

Using Visual Studio, create a project using the Console Application template.
Required:
Create class Book. Create classes Title, Author and Content, each of which must contain one string field and a void Show () method.
Implement the ability to add the book title, author name and content to the book.
Use the Show () method to display the title of the book, the name of the author, and the table of contents using the Show () method.

Solution:

+We create 3 classes - Author.cs, Title.cs and Content.cs
+In the body of each class, we create a private field and a method with the class name. In the body of the method, we assign a value as an argument to the method.
+In the body of the Author, Title and Content classes, create another show method and display the value of the variables on the screen.
+In the Book.cs file, we collect the book: we create three fields with our values ​​and in the Book method we create an instance of each class.
+Then we call our three classes in the Show method.
+In the Program.cs file, we create an instance of our book class and pass the values ​​in the argument.