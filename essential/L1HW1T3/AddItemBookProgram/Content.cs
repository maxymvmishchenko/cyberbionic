﻿using System;

namespace AddItemBookProgram
{
    //create class Content
    public class Content
    {
        private readonly string _content;

        public Content(string content)
        {
            _content = content;
        }

        public void Show()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Content: {0}", _content);

            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(new string ('-',75));

            Console.WriteLine("Thanks for watching");


        }
    }
}
