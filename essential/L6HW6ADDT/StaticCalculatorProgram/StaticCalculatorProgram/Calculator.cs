﻿using System;
using System.Diagnostics.Eventing.Reader;

namespace StaticCalculatorProgram
{
    static class Calculator
    {
        //Sum Method
        public static double Sum(double a, double b)
        {
            return a + b;
        }

        //Subtraction Method
        public static double Subtraction(double a, double b)
        {
            return a - b;
        }

        //Division Method
        public static double Division(double a, double b)
        {
            if (a == 0 || b == 0)
            {
                return 0;
            }
            else
                return a / b;
        }

        //Multiplication Method
        public static double Multiplication(double a, double b)
        {
            return a * b;
        }
    }
}
