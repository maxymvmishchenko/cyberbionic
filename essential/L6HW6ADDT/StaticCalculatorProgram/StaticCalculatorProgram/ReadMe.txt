Task 2
Using Visual Studio, create a project using the Console Application template.
Required:
Create a static Calculator class, with methods for performing basic arithmetic operations.
Write a program that displays basic arithmetic operations.

Solution:

+ Create a static class Calculate.

+ In the body of the class, we will create 5 static methods that take two integer arguments and return an int value: Addition, Subtraction, Multiplication, Division.

+ In the methods I will write down the logic of arithmetic operations according to the method.

+ In the Program class, in the Main method, create two integer variables and assign values ​​to them.

+ On the object class, I will call all 4 methods.

+ I will display the results of the arithmetic operations on the screen.