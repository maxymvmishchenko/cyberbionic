﻿using System;

namespace StaticCalculatorProgram
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine(Calculator.Sum(20, 10.5));
            Console.WriteLine(Calculator.Subtraction(13.2, 5));
            Console.WriteLine(Calculator.Multiplication(3.2, 5.8));

            if (Calculator.Division(20, 10) != 0)
            {
                Console.WriteLine(Calculator.Division(20, 10));
            }
            else
            { 
                Console.WriteLine("Попытка деления на 0");
            }
            

            Console.ReadKey();
        }
    }
}
