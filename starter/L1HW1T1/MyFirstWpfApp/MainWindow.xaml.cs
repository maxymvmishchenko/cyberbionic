﻿using System.Windows;

namespace MyFirstWpfApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        /// <summary>
        /// Creates a instance of the MainWindow
        /// <remarks>Something</remarks>
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Some action
        /// </summary>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            TextBox2.Text = TextBox1.Text;
        }
    }
}