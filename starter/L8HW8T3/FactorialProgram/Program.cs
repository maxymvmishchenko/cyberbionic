﻿using System;

namespace FactorialProgram
{
    class Program
    {
        static int Factorial(int n)
        {
            if (n == 0)
                return 1;
            else
                return n * Factorial(n - 1);
        }


        static void Main()
        {
            int factorial = Factorial(4);
            Console.WriteLine("Possible delivery options: {0}", factorial);

            Console.ReadKey();
        }
    }
}
