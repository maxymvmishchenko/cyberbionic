Assignment 3
There are N customers to whom the manufacturer must deliver the goods. How many possible delivery routes are there, taking into account that the goods will be delivered by one machine?
Using Visual Studio, create a project using the Console Application template.
Write a program that will calculate and display the number of possible delivery options for a product. To solve the problem, use the factorial N !, calculated using recursion. Explain why it is not recommended to use recursion to calculate factorial. Indicate the weak points of this approach.

Solution:
1. Create a Factorial method that takes an integer value and returns an integer type.
2. Using the If construct, we check the value entered by the user. If it is 0, then we return 1. Then we calculate the factorial for the formula.

1.In the Main method, we accept an integer number from the user. We call the Factorial method and pass the value entered by the user.
2.Display the result of calculations on the screen