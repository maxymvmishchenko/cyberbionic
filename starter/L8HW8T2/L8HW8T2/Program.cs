﻿using System;
using System.Text;

namespace L8HW8T2
{
    class Program
    {
        static void Credit(ref decimal sum)
        {   //body credits
            decimal bodyCredit = 700;
            Console.WriteLine("Client paid now - {0}", sum);

            decimal difference = bodyCredit - sum;
            Console.WriteLine("Load debt: {0}", difference);

            //overpayment
            if (difference < 0)
            {
                Console.WriteLine("Overpayment: {0} ", difference);
            }

            else if (difference == 0)
            {
                Console.WriteLine("All credit paid");
            }
            else
            {
                Console.WriteLine("Line mill credit: {0}", difference);
            }
            //delay
            Console.ReadKey();
        }

        static void Main()
        {
           Console.Write("Enter the payment amount: ");
            decimal amountPay = Convert.ToDecimal(Console.ReadLine());
            Credit(ref amountPay);

        }
    }
}
