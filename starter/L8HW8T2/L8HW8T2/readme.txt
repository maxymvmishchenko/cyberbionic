Assignment 2
Using Visual Studio, create a project using the Console Application template.
Imagine that you are implementing a program for a bank that helps to determine whether a client has repaid a loan or not. Let's say the monthly payment amount should be 100 UAH. The client must complete 7 payments, but may pay less often in large amounts. That is, it can be in two payments of 300 and 400 UAH. Close all debt.
Create a method that will accept the payment amount entered by the bank economist as an argument. The method displays information about the status of the loan (the amount owed, the amount of overpayment, a message about no debt).

Solution:
1. Create a credit method that takes one decimal variable and returns empty. In the body of the method, create a variable for the body of the loan, a variable that will receive the remainder after entering the payment amounts.
2. Using the if construction, we check the remainder of the loan amount, as well as the overpayment, if any.
3. We display the result on the screen.

1. In the Main method, we accept a value from the user and convert it to a currency type - decimal.

2. We call the Credit method and pass our value there, which the user enters.