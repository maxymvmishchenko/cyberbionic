﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvenOddTwoArrays
{
    class Program
    {
        static void Main(string[] args) // Результат: 2 массива, в одном четные числа из массивов, в другом - нечет
        {
            int[] array1 =
            {
                1, 2, 4,7, 9, 11, 12
            };

            int[] array2 =
            {
                3, 5, 6, 7, 8, 9, 10
            };

            int[] array3 =
            {
                2, 3, 6, 12, 13, 17, 18
            };

            //add length one plus two arrays
            int[] array4 = new int[array1.Length + array2.Length];

            //add length two plus three arrays
            int[] array5 = new int[array4.Length + array3.Length];

            //fill array even numbers 
            int[] evenArray = new int[10];

            //fill array odd numbers
            int[] oddArray = new int[11];
            
            for (int j = 0, a1Index = 0, a2Index = 0; j < array4.Length; j++)
            {
                if (a2Index < array2.Length && array1[a1Index] > array2[a2Index])
                {
                    array4[j] = array2[a2Index++];
                    continue;
                }

                array4[j] = array1[a1Index++];
            }

            for (int i = 0, a3Index = 0, a4Index = 0; i < array5.Length; i++)
            {
                if (a4Index < array4.Length && array3[a3Index] > array4[a4Index])
                {
                    array5[i] = array4[a4Index++];
                    continue;
                }
                array5[i] = array3[a3Index++];
            }

            for (int k = 0, n = 0, p = 0; k < array5.Length; k++)
            {
                if (array5[k] % 2 == 0)
                {
                    //fill array even numbers
                    evenArray[n++] = array5[k];
                }
                else if (array5[k] % 2 != 0)
                {
                    //fill array odd numbers
                    oddArray[p++] = array5[k];
                }
            }

            Console.ReadKey();
        }
    }
}
