﻿using System;


namespace ArraysTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array1 = new int[]
            {
                1, 5, 6
            };

            int[] array2 = new int[]
            {
                2,3,4,7,8,9,10
            };

            int[] array3 = new int[array1.Length + array2.Length];

            for (int i = 0, a1Index = 0, a2Index = 0; i < array3.Length; i++)
            {
                if (a1Index< array1.Length && array1[a1Index] < array2[a2Index])
                {
                    array3[i] = array1[a1Index++];
                    continue;
                }

                array3[i] = array2[a2Index++];

            }

            Console.ReadKey();
        }
    }
}
