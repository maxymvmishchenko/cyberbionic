﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReverseArrays
{
    class Program
    {
        static void Main(string[] args) // Результат: 3 массива с реверсивными елементами
        {
            int[] array1 =
            {
                1, 2, 4, 7, 9, 11, 12
            };

            int[] array2 =
            {
                3, 5, 6, 7, 8, 9, 10
            };

            int[] array3 =
            {
                2, 3, 6, 12, 13, 17, 18
            };

            int[] array = new int [array1.Length];

            for (int i = array1.Length - 1, n = 0; n < array1.Length; i-- )
            {
                array[n++] = array1[i];
            }

            foreach (var elem in array)

            {
                Console.WriteLine(elem);
            }
            Console.ReadKey();
        }
    }
}
