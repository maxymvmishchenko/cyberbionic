﻿using System;
using System.Collections.Generic;
using Telegram.Bot;
using System.Globalization;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace TelegramBot
{
    class Program
    {
        private static string token { get; set; } = "1526663084:AAEhXUQJlmG6XbKvaKL8WACtw0yrK3AmGGc";
        private static TelegramBotClient client;
        static void Main(string[] args)
        {
            client = new TelegramBotClient(token);
            client.StartReceiving();
            client.OnMessage += OnMessageHandler;
            Console.ReadKey();
            client.StartReceiving();
        }

        private static async void OnMessageHandler(object? sender, MessageEventArgs e)
        {
            var msg = e.Message;
            if (msg.Text != null)
            {
                Console.WriteLine($"Пришло сообщение с текстом: {msg.Text}");
                switch (msg.Text)
                {
                    case "Hello":
                        var hello = await client.SendStickerAsync(
                            chatId: msg.Chat.Id,
                            sticker: "https://tlgrm.ru/_/stickers/8c0/427/8c042720-c3bc-4b75-997e-25b666d88da4/4.webp",
                            replyToMessageId: msg.MessageId,
                            replyMarkup: GetButtons());
                        break;
                    case "Sticker":
                        var stick = await client.SendStickerAsync(
                            chatId: msg.Chat.Id,
                            sticker: "https://tlgrm.ru/_/stickers/8c0/427/8c042720-c3bc-4b75-997e-25b666d88da4/1.webp",
                            replyToMessageId: msg.MessageId,
                            replyMarkup: GetButtons());
                        break;

                    case "Hobby":
                        var hobby = await client.SendStickerAsync(
                            chatId: msg.Chat.Id,
                            sticker: "https://tlgrm.ru/_/stickers/8c0/427/8c042720-c3bc-4b75-997e-25b666d88da4/192/15.webp",
                            replyToMessageId: msg.MessageId,
                            replyMarkup: GetButtons());
                        break;

                    case "Bye":
                        var bye = await client.SendStickerAsync(
                            chatId: msg.Chat.Id,
                            sticker: "https://tlgrm.ru/_/stickers/8c0/427/8c042720-c3bc-4b75-997e-25b666d88da4/192/48.webp",
                            replyToMessageId: msg.MessageId,
                            replyMarkup: GetButtons());
                        break;

                    default:
                        await client.SendTextMessageAsync(msg.Chat.Id, "Input button: ", replyMarkup: GetButtons());
                        break;
                }

                //await client.SendTextMessageAsync(msg.Chat.Id, msg.Text, replyMarkup: GetButtons());
            }
        }

        private static IReplyMarkup GetButtons()
        {
            return new ReplyKeyboardMarkup
            {
                Keyboard = new List<List<KeyboardButton>>
                {
                    new List<KeyboardButton>{ new KeyboardButton{Text = "Hello"}, new KeyboardButton { Text = "Sticker"}},
                    new List<KeyboardButton>{ new KeyboardButton { Text = "Hobby"}, new KeyboardButton{ Text = "Bye" } }

                }
            };
        }
    }
}
