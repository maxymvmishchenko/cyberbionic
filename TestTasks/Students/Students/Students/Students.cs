﻿namespace Students
{
    public class Students
    {
        public string Name { get; set;}
        public int Age { get; set;}
        public int Course { get; set;}
        public string Specialty { get; set; }


        public Students(string name, int age, int course, string specialty)
        {
            Name = name;
            Age = age;
            Course = course;
            Specialty = specialty;
        }
    }
}