﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Students
{
    class Services
    {
        private readonly List<Students> students = new List<Students>();

        public void AddStudent(string name, int age, int course, string specialty)
        {
            students.Add(new Students(name, age, course, specialty));
        }

        public void DisplayAll()
        {
            foreach (var i in students)
            {
                Console.WriteLine($"{i.Name} - {i.Age} - {i.Course} - {i.Specialty}");
            }
        }

        public void GetByAge(int more, int less)
        {
            var getByAge = students.Where(age => age.Age > more && age.Age < less);
           
            foreach (var i in getByAge)
            {
                Console.WriteLine($"{i.Name}");
            }
        }

        public void GetByCourseSpecialty(int course, string specialty)
        {
            var getByCourseSpecialty = students.Where(courseSpecialty =>
                courseSpecialty.Course == course && courseSpecialty.Specialty == specialty);

            foreach (var i in getByCourseSpecialty)
            {
                Console.WriteLine($"{i.Name} - {i.Course} - {i.Specialty}");
            }
        }

        public void DeleteByName(string name)
        {
            var deletebyName = students.Where(deleteByName => deleteByName.Name == name).Select(x => x).First();
            students.Remove(deletebyName);
        }
    }
}
