﻿using System;

namespace Students
{
    class Program
    {
        static void Main(string[] args)
        {

            var services = new Services();

            services.AddStudent("Maks", 15, 1, "mechanic");
            services.AddStudent("Anton", 18, 2, "programmer");
            services.AddStudent("Artur", 17, 2, "programmer");
            services.AddStudent("Oksana", 20, 5, "programmer");
            services.AddStudent("Sasha", 17, 2, "tehnolog");
            services.AddStudent("Vadim", 22, 5, "yurist");

            services.DisplayAll();
            services.GetByAge(17, 22);

            services.GetByCourseSpecialty(2, "programmer");
            services.DeleteByName("Vadim");
            services.DisplayAll();

            Console.ReadKey();
        }
    }
}
