﻿using System;

namespace FirmProgram.Models
{
    /// <summary>
    /// Contains data for the worker
    /// </summary>
    public class Worker : IWorker
    {
        /// <summary>
        /// Gets or sets the name of the employee
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the experience of the employee
        /// </summary>
        public int Experience { get; set; }

        /// <summary>
        /// Shows the "Give products" message
        /// </summary>
        public void Job()
        {
            Console.WriteLine("Give products");
        }
    }
}