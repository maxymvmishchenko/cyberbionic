﻿namespace FirmProgram.Models
{
    /// <summary>
    /// Contains data for the foreman
    /// </summary>
    public interface IForeman : IEmployee
    {
        /// <summary>
        /// Checks workers
        /// </summary>
        void CheckWorker();
    }
}