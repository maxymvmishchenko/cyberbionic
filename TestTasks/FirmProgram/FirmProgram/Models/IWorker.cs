﻿namespace FirmProgram.Models
{
    /// <summary>
    /// Contains data for the workers
    /// </summary>
    public interface IWorker : IEmployee
    {
        // The current interface contains no members
    }
}