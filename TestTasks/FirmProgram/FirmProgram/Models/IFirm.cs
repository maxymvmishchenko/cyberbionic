﻿using System.Collections.Generic;

namespace FirmProgram.Models
{
    /// <summary>
    /// Contains logic which provides working with employees
    /// </summary>
    public interface IFirm
    {
        /// <summary>
        /// Gets all employees int he firm
        /// </summary>
        IList<IEmployee> Employees { get; }

        /// <summary>
        /// Gets employees by the required type
        /// </summary>
        /// <typeparam name="T">Type of the needed employees</typeparam>
        /// <returns>Employees by the required type</returns>
        IEnumerable<IEmployee> GetEmployees<T>() where T : IEmployee;

        /// <summary>
        /// Gets number of employees by the required type
        /// </summary>
        /// <typeparam name="T">Type of the needed employees</typeparam>
        /// <returns>Number of employees by the required type</returns>
        int GetCountOfEmployees<T>() where T : IEmployee;
    }
}