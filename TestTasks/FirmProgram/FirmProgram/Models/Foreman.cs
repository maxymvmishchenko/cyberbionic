﻿using System;

namespace FirmProgram.Models
{
    /// <summary>
    /// Contains data for the foreman
    /// </summary>
    public class Foreman : IForeman
    {
        /// <summary>
        /// Gets or sets the name of the employee
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the experience of the employee
        /// </summary>
        public int Experience { get; set; }

        /// <summary>
        /// Purchases of materials
        /// </summary>
        public void Job()
        {
            Console.WriteLine("Purchase of materials");
        }

        /// <summary>
        /// Checks workers
        /// </summary>
        public void CheckWorker()
        {
            Console.WriteLine("Foreman check workers");
        }
    }
}