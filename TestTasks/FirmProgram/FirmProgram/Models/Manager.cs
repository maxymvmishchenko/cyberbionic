﻿using System;

namespace FirmProgram.Models
{
    /// <summary>
    /// Contains data for the managers
    /// </summary>
    public class Manager : IManager
    {
        /// <summary>
        /// Gets or sets the name of the employee
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the experience of the employee
        /// </summary>
        public int Experience { get; set; }

        /// <summary>
        /// Shows message about collecting orders
        /// </summary>
        public void Job()
        {
            Console.WriteLine("Сollecting orders");
        }

        /// <summary>
        /// Shows the "Manager give work" message
        /// </summary>
        public void GiveWork()
        {
            Console.WriteLine("Manager give work");
        }
    }
}