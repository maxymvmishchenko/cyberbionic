﻿namespace FirmProgram.Models
{
    /// <summary>
    /// Interface which provides data for a employee
    /// </summary>
    public interface IEmployee
    {
        /// <summary>
        /// Gets or sets the name of the employee
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the experience of the employee
        /// </summary>
        int Experience { get; set; }

        /// <summary>
        /// Does something
        /// </summary>
        void Job();
    }
}