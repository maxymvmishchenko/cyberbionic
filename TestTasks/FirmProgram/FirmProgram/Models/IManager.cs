﻿namespace FirmProgram.Models
{
    /// <summary>
    /// Contains data for the managers
    /// </summary>
    public interface IManager : IEmployee
    {
        /// <summary>
        /// Gives work for employees
        /// </summary>
        void GiveWork();
    }
}