﻿using System.Collections.Generic;
using System.Linq;
using FirmProgram.Models;

namespace FirmProgram.Entities
{
    /// <summary>
    /// Contains logic which provides working with employees
    /// </summary>
    public class Firm : IFirm
    {
        #region PrivateMembers

        private readonly IList<IEmployee> _employees;

        #endregion

        #region Constructors
        
        /// <summary>
        /// Creates instance of the <see cref="Firm"/>
        /// </summary>
        public Firm()
        {
            _employees = new List<IEmployee>();
        }

        #endregion

        #region PublicMembers

        /// <summary>
        /// Gets all employees int the firm
        /// </summary>
        public IList<IEmployee> Employees => _employees;

        /// <summary>
        /// Gets employees by the required type
        /// </summary>
        /// <typeparam name="T">Type of the needed employees</typeparam>
        /// <returns>Employees by the required type</returns>
        public IEnumerable<IEmployee> GetEmployees<T>() where T : IEmployee
        {
            if (typeof(IManager) == typeof(T) || typeof(Manager) == typeof(T))
            {
                return _employees.Where(employee => employee is IManager);
            }
            if (typeof(IForeman) == typeof(T) || typeof(Foreman) == typeof(T))
            {
                return _employees.Where(employee => employee is IForeman);
            }
            if (typeof(IWorker) == typeof(T) || typeof(Worker) == typeof(T))
            {
                return _employees.Where(employee => employee is IWorker);
            }

            return null;
        }

        /// <summary>
        /// Gets number of employees by the required type
        /// </summary>
        /// <typeparam name="T">Type of the needed employees</typeparam>
        /// <returns>Number of employees by the required type</returns>
        public int GetCountOfEmployees<T>() where T : IEmployee
        {
            return _employees.Count(employee => employee is T);
        }

        /// <summary>
        /// Adds the employee to the firm
        /// </summary>
        /// <param name="currentFirm">The current firm</param>
        /// <param name="employee">The current employee to add</param>
        /// <returns>True if firm doesn't contain the employee and employee is successfully added,
        /// otherwise - false</returns>
        public static bool operator +(Firm currentFirm, IEmployee employee)
        {
            if (currentFirm.Employees.Contains(employee) || employee == null)
            {
                return false;
            }

            currentFirm.Employees.Add(employee);
            return true;
        }

        /// <summary>
        /// Removes the employee from the firm
        /// </summary>
        /// <param name="currentFirm">The current firm</param>
        /// <param name="employee">The current employee to remove</param>
        /// <returns>True if firm contains the employee and employee is successfully removed,
        /// otherwise - false</returns>
        public static bool operator -(Firm currentFirm, IEmployee employee)
        {
            if (!currentFirm.Employees.Contains(employee))
            {
                return false;
            }

            currentFirm.Employees.Remove(employee);
            return true;
        }

        #endregion
    }
}
