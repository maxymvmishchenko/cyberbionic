﻿using System;
using FirmProgram.Entities;
using FirmProgram.Extensions;
using FirmProgram.Models;

namespace FirmProgram
{
    /// <summary>
    /// Main class Program
    /// </summary>
    public class Program
    { 
        /// <summary>
        /// Main method
        /// </summary>
        static void Main()
        {
            var firm = new Firm();
            var manager = new Manager()
            {
                Name = "Anton",
                Experience = 13
            };
            var manager2 = new Manager()
            {
                Name = "Max",
                Experience = 15
            };
            var worker = new Worker()
            {
                Name = "Artur",
                Experience = 12
            };

            var foreman1 = new Worker()
            {
                Name = "Oksana",
                Experience = 3
            };


            Console.WriteLine($"{firm + manager}");
            Console.WriteLine($"{firm + manager2}");
            Console.WriteLine($"{firm + worker}");
            firm.ShowEmployees();

            var managers = firm.GetEmployees<Manager>();
            var countOfManagers = firm.GetCountOfEmployees<Manager>();

            Console.WriteLine(managers);
            Console.WriteLine(countOfManagers);

            var foreman = firm.GetEmployees<Foreman>();
            var countOfForeman = firm.GetCountOfEmployees<Foreman>();

            var worker1 = firm.GetEmployees<Worker>();
            var countOfWorker = firm.GetCountOfEmployees<Worker>();
            Console.ReadKey();
        }
    }
}