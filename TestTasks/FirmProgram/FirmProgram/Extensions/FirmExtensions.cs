﻿using System;
using System.Linq;
using FirmProgram.Entities;
using FirmProgram.Models;

namespace FirmProgram.Extensions
{
    /// <summary>
    /// Firm extensions
    /// </summary>
    public static class FirmExtensions
    {
        /// <summary>
        /// Checks is employee exist in the current firm
        /// </summary>
        /// <param name="currentFirm">The current firm</param>
        /// <param name="currentEmployee">The current employee</param>
        /// <returns>True if the employee is exist in the firm</returns>
        public static bool EmployeeIsExist(this Firm currentFirm, IEmployee currentEmployee)
        {
            return currentFirm.Employees.Any(employee => employee.Equals(currentEmployee));
        }

        /// <summary>
        /// Shows all employees in the firm
        /// </summary>
        /// <param name="currentFirm">The current firm</param>
        public static void ShowEmployees(this Firm currentFirm)
        {
            Console.WriteLine("Employees: ");
            foreach (var employee in currentFirm.Employees)
            {
                Console.WriteLine($"Name - {employee.Name}; Experience - {employee.Experience};");
            }
        }
    }
}