﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Shop.Data.Models;

namespace Shop.Data
{
    public class DbObjects
    {
        public static void Initial(AppDbContent content)
        {
            if (!content.Category.Any())
                content.Category.AddRange(Categories.Select(c => c.Value));

            if (!content.Car.Any())
            {
                content.AddRange(
                    new Car
                    {
                        Name = "Tesla",
                        ShortDesc = "Быстрый автомобиль",
                        LongDesc = "Быстрый, надежный и качественный автомобиль от компании Tesla",
                        Img = "/images/Tesla.jpg",
                        Price = 45000,
                        IsFavorite = true,
                        Available = true,
                        Category = Categories["Електроавтомобили"]
                    },

                    new Car
                    {
                        Name = "Ford Fiesta",
                        ShortDesc = "Тихий и спокойный",
                        LongDesc = "Удобный автомобиль для городской жизни",
                        Img = "/images/Ford.jpg",
                        Price = 10000,
                        IsFavorite = true,
                        Available = true,
                        Category = Categories["Классические автомобили"]
                    },

                    new Car
                    {
                        Name = "BMW",
                        ShortDesc = "Дерзский и стильный",
                        LongDesc = "Удобен для городской жизни",
                        Img = "/images/bmw.jpg",
                        Price = 20000,
                        IsFavorite = true,
                        Available = false,
                        Category = Categories["Классические автомобили"]
                    },

                    new Car
                    {
                        Name = "Mercedes",
                        ShortDesc = "Уютный и стильный",
                        LongDesc = "Подходит длягородской езды",
                        Img = "/images/Mercedes.jpg",
                        Price = 30000,
                        IsFavorite = true,
                        Available = false,
                        Category = Categories["Классические автомобили"]
                    },

                    new Car
                    {
                        Name = "Nissan Leaf",
                        ShortDesc = "Японский електромобиль",
                        LongDesc = "Надежный, быстрый и тихий Nissan Leaf",
                        Img = "/images/Nissan.jpg",
                        Price = 15000,
                        IsFavorite = true,
                        Available = true,
                        Category = Categories["Електроавтомобили"]
                    });
            }

            content.SaveChanges();

        }

        private static Dictionary<string, Category> _category;
        public static Dictionary<string, Category> Categories
        {
            get
            {
                if (_category == null)
                {
                    var list = new Category[]
                    {
                        new Category {CategoryName = "Електроавтомобили", Desc = "Современный вид траснпорта"},
                        new Category {CategoryName = "Классические автомобили", Desc = "Автомобили с двигателем внутреннего сгорания"}
                    };
                    _category = new Dictionary<string, Category>();
                    foreach (Category el in list)
                        _category.Add(el.CategoryName, el);
                }

                return _category;
            }
        }
    }
}
