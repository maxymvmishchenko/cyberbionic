﻿using System;
using System.Collections.Generic;
using System.Linq;
using Shop.Data.Interfaces;
using Shop.Data.Models;

namespace Shop.Data.Mocks
{
    public class MockCars : IAllCars
    {
        private readonly ICarsCategory _categoryCars = new MockCategory();
        public IEnumerable<Car> Cars
        {
            get
            {
                return new List<Car>
                {
                    new Car { Name = "Tesla",
                        ShortDesc = "Быстрый автомобиль",
                        LongDesc = "Быстрый, надежный и качественный автомобиль от компании Tesla",
                        Img = "/images/Tesla.jpg",
                        Price = 45000,
                        IsFavorite =  true,
                        Available = true,
                        Category = _categoryCars.AllCategories.First()},

                    new Car { Name = "Ford Fiesta",
                        ShortDesc = "Тихий и спокойный",
                        LongDesc = "Удобный автомобиль для городской жизни",
                        Img = "/images/Ford.jpg",
                        Price = 10000,
                        IsFavorite =  false,
                        Available = true,
                        Category = _categoryCars.AllCategories.Last()},

                    new Car { Name = "BMW",
                        ShortDesc = "Дерзский и стильный",
                        LongDesc = "Удобен для городской жизни",
                        Img = "/images/bmw.jpg",
                        Price = 20000,
                        IsFavorite =  false,
                        Available = false,
                        Category = _categoryCars.AllCategories.Last()},

                    new Car { Name = "Mercedes",
                        ShortDesc = "Уютный и стильный",
                        LongDesc = "Подходит длягородской езды",
                        Img = "/images/Mercedes.jpg",
                        Price = 30000,
                        IsFavorite =  true,
                        Available = false,
                        Category = _categoryCars.AllCategories.Last()},

                    new Car { Name = "Nissan Leaf",
                        ShortDesc = "Японский електромобиль",
                        LongDesc = "Надежный, быстрый и тихий Nissan Leaf",
                        Img = "/images/Nissan.jpg",
                        Price = 15000,
                        IsFavorite =  true,
                        Available = true,
                        Category = _categoryCars.AllCategories.First()}
                };
            }

        }
        public IEnumerable<Car> GetFavCars { get; set; }

        public Car GetobjectCar(int carId)
        {
            throw new NotImplementedException();
        }
    }
}
