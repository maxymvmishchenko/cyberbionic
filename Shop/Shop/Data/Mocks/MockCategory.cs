﻿using System.Collections.Generic;
using Shop.Data.Interfaces;
using Shop.Data.Models;

namespace Shop.Data.Mocks
{
    public class MockCategory : ICarsCategory
    {
        public IEnumerable<Category> AllCategories
        {
            get
            {
               return new List<Category>()
                {
                    new Category{CategoryName = "Електроавтомобили", Desc = "Современный вид траснпорта"},
                    new Category{CategoryName = "Классические автомобили", Desc = "Автомобили с двигателем внутреннего сгорания"}

                };
            }
        }
    }
}
