﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Shop.Data.Interfaces;
using Shop.Data.Models;

namespace Shop.Data.Repository
{
    public class CarsRepository : IAllCars
    {
        private readonly AppDbContent _appDbContent;

        public CarsRepository(AppDbContent appDbContent)
        {
            _appDbContent = appDbContent;
        }

        public IEnumerable<Car> Cars => _appDbContent.Car.Include(c => c.Category);

        public IEnumerable<Car> GetFavCars => _appDbContent.Car.Where(c => c.IsFavorite).Include(p => p.Category);

        public Car GetobjectCar(int carId) => _appDbContent.Car.FirstOrDefault(p => p.Id == carId);
    }
}
