﻿using Microsoft.AspNetCore.Mvc;
using Shop.Data.Interfaces;
using Shop.ViewModels;

namespace Shop.Controllers
{
    public class CarsController : Controller
    {
        private readonly IAllCars _allCars;
        private readonly ICarsCategory _allCategories;

        public CarsController(IAllCars iAllCars, ICarsCategory iAllCategories)
        {
            _allCars = iAllCars;
            _allCategories = iAllCategories;
        }

        public ViewResult List()
        {
            CarListViewModel obj = new CarListViewModel();
            ViewBag.Title = "Страница с автомобилями";
            obj.AllCars = _allCars.Cars;
            obj.currCategory = "Автомобили";
            
            return View(obj);
        }
    }
}
